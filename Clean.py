from utils import (load_data, clean_income, clean_pref_movie,
                   count_people_by_gender, count_people_looking_for,
                   display_count, calculate_average_income, calculate_median_income)


def people_with_high_income(people, limit):
    return [p for p in people if clean_income(p['income']) > limit]


def people_who_like_genre(people, genre):
    return [p for p in people if genre in clean_pref_movie(p['pref_movie'])]


class PeopleAnalytics:

    def women_who_like_scifi(self, people):
        return [p for p in people if p['gender'] == 'Female' and 'Sci-Fi' in clean_pref_movie(p['pref_movie'])]

    def people_who_like_documentaries_and_win_over_1482(self, people):
        return [p for p in people if
                "Documentary" in clean_pref_movie(p['pref_movie']) and clean_income(p['income']) > 1482]

    def list_people_who_win_over_4000(self, people):
        high_earners = [p for p in people if clean_income(p['income']) > 4000]
        return [(p['first_name'], p['last_name'], p['id'], p['income']) for p in high_earners]

    def richest_man(self, people):
        men = [p for p in people if p['gender'] == 'Male']
        richest_men = sorted(men, key=lambda x: clean_income(x['income']), reverse=True)
        return richest_men[0]['first_name'], richest_men[0]['last_name'], richest_men[0]['id'], richest_men[0]['income']


if __name__ == '__main__':
    analytics = PeopleAnalytics()
    people = load_data('people.json')
    display_count("Nombre d'hommes", count_people_by_gender(people, 'Male'))
    display_count("Nombre de femmes", count_people_by_gender(people, 'Female'))
    display_count("Nombre de personnes qui cherchent un homme", count_people_looking_for(people, 'M'))
    display_count("Nombre de personnes qui cherchent une femme", count_people_looking_for(people, 'F'))
    display_count("Nombre de personnes qui gagnent plus de 2000$", len(people_with_high_income(people, 2000)))
    for p in analytics.people_who_like_documentaries_and_win_over_1482(people):
        print(
            f" personne qui aiment les documentaires et qui gagnent plus de 1482$: {p['first_name']} {p['last_name']} "
            f"(ID: {p['id']}) - Revenu: {p['income']}")
    display_count("Nombre de personnes qui aiment les Drama", len(people_who_like_genre(people, 'Drama')))
    display_count("Nombre de femmes qui aiment la science-fiction", len(analytics.women_who_like_scifi(people)))
    high_earners_list = analytics.list_people_who_win_over_4000(people)
    print("Liste des personnes qui gagnent plus de 4000$:")
    for first_name, last_name, id, income in high_earners_list:
        print(f"{first_name} {last_name} (ID: {id}) - Revenu: {income}")
    first_name, last_name, id, income = analytics.richest_man(people)
    print(f"L'homme le plus riche est : {first_name} {last_name} (son ID est : {id}) avec un revenu de {income}")
    avg_income = calculate_average_income(people)
    print(f"Le salaire moyen est de ${avg_income:.2f}")
    med_income = calculate_median_income(people)
    print(f"Le salaire médian est de ${med_income:.2f}")
