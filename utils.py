import json
from termcolor import colored
import statistics


def load_data(json_file):
    with open(json_file, 'r') as p:
        return json.loads(p.read())


def clean_income(income):
    return float(income.replace('$', '').replace(',', ''))


def clean_pref_movie(pref_movie):
    return pref_movie.replace('|', '').split(',')


def count_people_by_gender(people, gender):
    return len([p for p in people if p['gender'] == gender])


def count_people_looking_for(people, looking_for):
    return len([p for p in people if p['looking_for'] == looking_for])


def display_count(message, count):
    print(colored(f"{message}: {count}", 'yellow'))


def calculate_average_income(people):
    total_income = sum([clean_income(p['income']) for p in people])
    return total_income / len(people) if len(people) > 0 else 0


def calculate_median_income(people):
    all_incomes = sorted([clean_income(p['income']) for p in people])
    return statistics.median(all_incomes) if len(all_incomes) > 0 else 0
