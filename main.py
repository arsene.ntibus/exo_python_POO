import json
import statistics
from pprint import pprint
from termcolor import colored

with open('people.json', 'r') as p:
    people = json.loads(p.read())

print(colored("""
$$$$$$$$\\        $$\\           $$\\                           $$\\                               
\\__$$  __|       \\__|          $$ |                          $$ |                              
   $$ | $$$$$$\\  $$\\ $$$$$$$\\  $$ |  $$\\  $$$$$$\\   $$$$$$\\  $$ | $$$$$$\\ $$\\    $$\\  $$$$$$\\  
   $$ |$$  __$$\\ $$ |$$  __$$\\ $$ | $$  |$$  __$$\\ $$  __$$\\ $$ |$$  __$$\\\\$$\\  $$  |$$  __$$\\ 
   $$ |$$ |  \\__|$$ |$$ |  $$ |$$$$$$  / $$$$$$$$ |$$ |  \\__|$$ |$$ /  $$ |\\$$\\$$  / $$$$$$$$ |
   $$ |$$ |      $$ |$$ |  $$ |$$  _$$<  $$   ____|$$ |      $$ |$$ |  $$ | \\$$$  /  $$   ____|
   $$ |$$ |      $$ |$$ |  $$ |$$ | \\$$\\ \\$$$$$$$\\ $$ |$$\\   $$ |\\$$$$$$  |  \\$  /   \\$$$$$$$\\ 
   \\__|\\__|      \\__|\\__|  \\__|\\__|  \\__| \\_______|\\__|\\__|  \\__| \\______/    \\_/     \\_______|
================================================================================================
   
""", 'yellow'))
print(colored('Modele des données :', 'yellow'))
print(people[0])

# debut de l'exo
print(colored(''.join(['_' for _ in range(80)]), 'green', 'on_green'))

print(colored("Nombre d'hommes : ", 'yellow'))
# pour chaque personne du tableau, si son genre == 'Male' je le met dans le tableau hommes
hommes = [p for p in people if p['gender'] == 'Male']
femmes = [p for p in people if p['gender'] == 'Female']

# len() revoie la taille (nombre d'élément) d'un tableau
print(len(hommes))
print(len(femmes))

################################################################################

# je peux aussi l'écrire avec une boucle classique
hommes2 = []  # un tableau vide
for person in people:  # pour chaque persone du tableau
    if person["gender"] == "Male":  # si c'est un homme (2-266-02250-4)
        hommes2.append(person)  # je l'ajoute au tableau
print(len(hommes2))

################################################################################

# dans la même idée, plutot que de mettre tous les hommes dans un tableau
# puis afficher la longueur du tableau, je peux juste les compter dans une variable
nb_hommes = 0  # je commence à 0
for person in people:  # pour chaque persone du tableau
    if person["gender"] == "Male":  # si c'est un homme
        nb_hommes = nb_hommes + 1  # j'ajoute 1 à mon compteur
print(nb_hommes)

################################################################################
nb_femmes = 0
for person in people:
    if person["gender"] == "Female":
        nb_femmes = nb_femmes + 1
print(nb_femmes)

# je peux compter les femmes ou calculer : nombre d'élement dans people - nombre d'homme
print(len(people) - len(hommes))
print(colored('A IMPLEMENTER', 'red', 'on_yellow'))
################################################################################

print(colored("Nombre de personnes qui cherchent un homme :", 'yellow'))
lookingforMale = []
for person in people:
    if person["looking_for"] == "M":
        lookingforMale.append(person)
print(len(lookingforMale))

print(colored('A IMPLEMENTER', 'red', 'on_yellow'))

################################################################################

print(colored("Nombre de personnes qui cherchent une femme :", 'yellow'))
lookingForFemale = []
for person in people:
    if person["looking_for"] == "F":
        lookingForFemale.append(person)
print(len(lookingForFemale))
print(colored('A IMPLEMENTER', 'red', 'on_yellow'))

################################################################################
print(colored("Nombre de personnes qui gagnent plus de 2000$ :", 'yellow'))


# là il va falloir regarder si le revenu est supérieur à 2000
def clean_income(income):
    """
    The code defines functions to clean and check income values, and then filters a list of people based
    on their income being over .
    
    :param income: The `income` parameter is a string representing a person's income
    :return: The function `get_people_with_income_over_2000` returns a list of dictionaries, where each
    dictionary represents a person with an income over .
    """
    income = income.replace('$', '')
    income = income.replace(',', '')
    income = float(income)
    return income


def is_income_over_2000(income):
    return income > 2000


def get_people_with_income_over_2000(people):
    lookingForMore2000 = []
    for person in people:
        income = clean_income(person["income"])
        if is_income_over_2000(income):
            lookingForMore2000.append(person)
    return lookingForMore2000


print(len(get_people_with_income_over_2000(people)))
print(colored('A IMPLEMENTER', 'red', 'on_yellow'))

# we loop through the list of people

print(colored('A IMPLEMENTER', 'red', 'on_yellow'))
################################################################################

print(colored("Nombre de personnes qui aiment les Drama :", 'yellow'))


def clean_pref_movie(pref_movie):
    pref_movie = pref_movie.replace('|', '')
    pref_movie = pref_movie.split(',')
    return pref_movie


def pref_movie_is_drama(pref_movie):
    return 'Drama' in pref_movie


def get_people_who_like_drama(people):
    lookingForDrama = []
    for person in people:
        pref_movie = clean_pref_movie(person["pref_movie"])
        if pref_movie_is_drama(pref_movie):
            lookingForDrama.append(person)
    return lookingForDrama

print(len(get_people_who_like_drama(people)))

print(colored('A IMPLEMENTER', 'red', 'on_yellow'))

################################################################################

print(colored("Nombre de femmes qui aiment la science-fiction :", 'yellow'))


def clean_pref_movie(pref_movie):
    pref_movie = pref_movie.replace('|', '')
    pref_movie = pref_movie.split(',')
    return pref_movie


def pref_movie_is_science_fiction(pref_movie):
    return 'Sci-Fi' in pref_movie


# dans le tableu des female  je cherche les femmes qui aiment la science fiction
femme_who_like_science_fiction = []
for person in femmes:
    pref_movie = clean_pref_movie(person["pref_movie"])
    if pref_movie_is_science_fiction(pref_movie):
        femme_who_like_science_fiction.append(person)
print(len(femme_who_like_science_fiction))
print(colored('A IMPLEMENTER', 'red', 'on_yellow'))

################################################################################

print(colored('LEVEL 2' + ''.join(['_' for _ in range(73)]), 'green', 'on_green'))

################################################################################

print(colored("Nombre de personnes qui aiment les documentaires et gagnent plus de 1482$", 'yellow'))


# je peux faire une fonction qui me renvoie le nombre de personnes qui aiment les documentaires et gagnent plus de 1482$
def clean_pref_movie(pref_movie):
    pref_movie = pref_movie.replace('|', '')
    pref_movie = pref_movie.split(',')
    return pref_movie


def pref_movie_is_documentary(pref_movie):
    return 'Documentary' in pref_movie


def clean_income(income):
    income = income.replace('$', '')
    income = income.replace(',', '')
    income = float(income)
    return income


def is_income_over_1482(income):
    return income > 1482


def get_people_who_like_documentary_and_income_over_1482(people):
    for person in people:
        pref_movie = clean_pref_movie(person["pref_movie"])
        if pref_movie_is_documentary(pref_movie):
            income = clean_income(person["income"])
            if is_income_over_1482(income):
                return person


print(len(get_people_who_like_documentary_and_income_over_1482(people)))
print(colored('A IMPLEMENTER', 'red', 'on_yellow'))

################################################################################

print(colored("Liste des noms, prénoms, id et revenus des personnes qui gagnent plus de 4000$", 'yellow'))

################################################################################

print(colored("Homme le plus riche (nom et id) :", 'yellow'))

print(colored('A IMPLEMENTER', 'red', 'on_yellow'))

################################################################################

print(colored("Salaire moyen :", 'yellow'))
# je peux faire une fonction qui me renvoie le salaire moyen
def clean_income(income):
print(colored('A IMPLEMENTER', 'red', 'on_yellow'))

################################################################################
print(colored("Salaire médian :", 'yellow'))

print(colored('A IMPLEMENTER', 'red', 'on_yellow'))

################################################################################

print(colored("Nombre de personnes qui habitent dans l'hémisphère nord :", 'yellow'))
print(colored('A IMPLEMENTER', 'red', 'on_yellow'))

################################################################################

print(colored("Salaire moyen des personnes qui habitent dans l'hémisphère sud :", 'yellow'))
print(colored('A IMPLEMENTER', 'red', 'on_yellow'))

################################################################################

print(colored('LEVEL 3' + ''.join(['_' for _ in range(73)]), 'green', 'on_green'))

################################################################################

print(colored("Personne qui habite le plus près de Bérénice Cawt (nom et id) :", 'yellow'))
print(colored('A IMPLEMENTER', 'red', 'on_yellow'))

################################################################################

print(colored("Personne qui habite le plus près de Ruì Brach (nom et id) :", 'yellow'))
print(colored('A IMPLEMENTER', 'red', 'on_yellow'))

################################################################################

print(colored("les 10 personnes qui habitent les plus près de Josée Boshard (nom et id) :", 'yellow'))
print(colored('A IMPLEMENTER', 'red', 'on_yellow'))

################################################################################

print(colored("Les noms et ids des 23 personnes qui travaillent chez google :", 'yellow'))
print(colored('A IMPLEMENTER', 'red', 'on_yellow'))

################################################################################

print(colored("Personne la plus agée :", 'yellow'))
print(colored('A IMPLEMENTER', 'red', 'on_yellow'))

################################################################################

print(colored("Personne la plus jeune :", 'yellow'))
print(colored('A IMPLEMENTER', 'red', 'on_yellow'))
print(colored("Moyenne des différences d'age :", 'yellow'))
print(colored('A IMPLEMENTER', 'red', 'on_yellow'))

################################################################################

print(colored('LEVEL 4' + ''.join(['_' for _ in range(73)]), 'green', 'on_green'))
print(colored("Genre de film le plus populaire :", 'yellow'))
print(colored('A IMPLEMENTER', 'red', 'on_yellow'))

################################################################################

print(colored("Genres de film par ordre de popularité :", 'yellow'))
print(colored('A IMPLEMENTER', 'red', 'on_yellow'))

################################################################################

print(colored("Liste des genres de film et nombre de personnes qui les préfèrent :", 'yellow'))
print(colored('A IMPLEMENTER', 'red', 'on_yellow'))

################################################################################

print(colored("Age moyen des hommes qui aiment les films noirs :", 'yellow'))
print(colored('A IMPLEMENTER', 'red', 'on_yellow'))

################################################################################

print(colored("Age moyen des femmes qui aiment les drames et habitent sur le fuseau horaire, de Paris : ", 'yellow'))
print(colored('A IMPLEMENTER', 'red', 'on_yellow'))

################################################################################

print(colored("""Homme qui cherche un homme et habite le plus proche d'un homme qui a au moins une
préférence de film en commun (afficher les deux et la distance entre les deux):""", 'yellow'))
print(colored('A IMPLEMENTER', 'red', 'on_yellow'))

################################################################################

print(colored("Liste des couples femmes / hommes qui ont les même préférences de films :", 'yellow'))
print(colored('A IMPLEMENTER', 'red', 'on_yellow'))

################################################################################

print(colored('MATCH' + ''.join(['_' for _ in range(73)]), 'green', 'on_green'))
"""
    On match les gens avec ce qu'ils cherchent (homme ou femme).
    On prend en priorité ceux qui ont le plus de gouts en commun.
    Puis ceux qui sont les plus proches.
    Les gens qui travaillent chez google ne peuvent qu'être en couple entre eux.
    Quelqu'un qui n'aime pas les Drama ne peux pas être en couple avec quelqu'un qui les aime.
    Quelqu'un qui aime les films d'aventure doit forcement être en couple avec quelqu'un qui aime aussi 
    les films d'aventure.
    La différences d'age dans un couple doit être inférieure à 25% (de l'age du plus agé des deux)
    ߷    ߷    ߷    Créer le plus de couples possibles.                  ߷    ߷    ߷    
    ߷    ߷    ߷    Mesurez le temps de calcul de votre fonction         ߷    ߷    ߷    
    ߷    ߷    ߷    Essayez de réduire le temps de calcul au maximum     ߷    ߷    ߷    

"""
print(colored("liste de couples à matcher (nom et id pour chaque membre du couple) :", 'yellow'))
print(colored('Exemple :', 'green'))
print(colored('1 Alice A.\t2 Bob B.'))
print(colored('A IMPLEMENTER', 'red', 'on_yellow'))
